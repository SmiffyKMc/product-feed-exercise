/*
 * processFeed is the entrypoint to this test but feel free
 * to break up your code in whatever way makes sense to you
*/
const parse = require('csv-parse');
const pipe = require('multipipe')
const transform = require('stream-transform');
const stringify = require('csv-stringify');
const zlib = require('zlib');
const fs = require('fs')
const gunzip = zlib.createGunzip();
const gzip = zlib.createGzip();

const readOpts = { highWaterMark: Math.pow(2, 24) };
const writeOpts = { highWaterMark: Math.pow(2, 24) };

let inputFile;
let outputFile;
let feeds = 0;
let removedFeeds = 0;
let prices = [];

const stringifyOutput = stringify({
  delimiter: ',',
  relax_column_count: true,
  skip_empty_lines: true,
  header: true,
});

const parser = parse({
  delimiter: ',',
  columns: true,
  fromLine: 1,
  relax_column_count: true
});

// Using transform here to catch the chunks passing through 
// and validating against the requested rules.
const transformer = transform(function (data) {
  if (data['availability'] === 'out of stock' || data['brand'] === 'Collier') {
    removedFeeds++;
    return null;
  } else {
    prices.push(parseInt(data['price']));
    feeds++;
    data['price'] = `$${data['price']} USD`;
    return data;
  }
});

async function processFeed(feedFilePath) {
  console.time('Process Time');
  console.log('Processing...');

  inputFile = fs.createReadStream(`${__dirname}/commerce-feed.csv.gz`, readOpts);
  outputFile = fs.createWriteStream(`${__dirname}/processed.csv.gz`, writeOpts);

  const stream = pipe(
    inputFile,
    gunzip,
    parser,
    transformer,
    stringifyOutput,
    gzip,
    outputFile);

  outputFile.on('finish', () => {
    printReport();
    closeStreams();
  });
  stream.on('error', (err) => {
    console.error('\x1b[31m%s\x1b[0m', `Processing Failed: ${err}`);
    closeStreams();
  });
}

function closeStreams() {
  inputFile.unpipe();
  outputFile.destroy();
}

function printReport() {
  prices = [...new Set(prices)];
  console.log('\x1b[32m%s\x1b[0m', 'Processing complete!');
  console.log('=======Results=======');
  console.log(`Total Row Count: ${feeds}`);
  console.log(`Removed Row Count: ${removedFeeds}`);
  console.log(`Max Price: ${Math.max(...prices)}`);
  console.log(`Min Price: ${Math.min(...prices)}`);
  console.timeEnd("Process Time");
}

processFeed();